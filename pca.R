#!/usr/bin/Rscript

#output suppressers
#sink("/dev/null")
suppressPackageStartupMessages(library("methods")) 

library(class)
library(ggplot2)


args <- commandArgs(TRUE)
dataFile <- args[1]
header <- args[2]

# comma delimited data and no header for each variable
#RawData <- read.table(data,sep = ",",header=FALSE)
if (header == "header") {
	RawData <- read.table(dataFile,sep = ",",header=TRUE)
} else {
	RawData <- read.table(dataFile,sep = ",",header=FALSE)
}

pca = princomp(RawData)
scores = pca$scores
pca.sdev = data.frame(output = (round(pca$sdev, 8)))
variance.pc.comp = round(pca.sdev^2 / sum(pca.sdev^2), 8)

pca.df <- data.frame()

write.csv(pca.sdev, "results.csv", row.names = FALSE, quote=FALSE)
write.csv(variance.pc.comp, "compVar.csv", row.names = FALSE, quote=FALSE)


png('plot.png')
plot(pca)

